from typing import List


class EasyWithConstructor:
    i_am_a_string: str
    i_am_an_int: int
    i_am_a_float: float
    i_am_a_list_with_strings: List[str]
    i_am_a_list_with_ints: List[int]

    def __init__(self,
                 i_am_a_string: str,
                 i_am_an_int: int,
                 i_am_a_float: float,
                 i_am_a_list_with_strings: List[str],
                 i_am_a_list_with_ints: List[int]):
        self.i_am_a_string = i_am_a_string
        self.i_am_an_int = i_am_an_int
        self.i_am_a_float = i_am_a_float
        self.i_am_a_list_with_strings = i_am_a_list_with_strings
        self.i_am_a_list_with_ints = i_am_a_list_with_ints

# import typing
# from typing import List
# from testfiles.EasyTypes import EasyWithConstructor
# asdf = typing.get_type_hints(EasyWithConstructor.__init__)
# for key in asdf:
#     value = asdf[key]
#     if value == str:
#         print("Dies ist ein String: %s" % key)
#     elif value == int:
#         print("Dies ist ein Int: %s" % key)
#     elif value == float:
#         print("Dies ist ein Float: %s" % key)
#     elif value._name == "List":
#         print("Dies ist eine Liste: %s" % key)
#         if value.__args__[0] == str:
#             print("    Und zwar mit Strings: %s" % key)
#         elif value.__args__[0] == int:
#             print("    Und zwar mit Strings: %s" % key)

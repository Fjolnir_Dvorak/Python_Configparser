from abc import abstractmethod
from typing import TypeVar

from configuration.ast.natives.AbstractNative import AbstractNative

T = TypeVar('T')

class AbstractType:
    @abstractmethod
    def get_representative(self):
        pass

    @abstractmethod
    def get_name(self) -> str:
        pass

    @abstractmethod
    def get_native(self) -> AbstractNative:
        pass

    @abstractmethod
    def createObject(self) -> T:
        pass

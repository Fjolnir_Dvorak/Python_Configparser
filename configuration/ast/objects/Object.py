from __future__ import annotations

import typing
from inspect import isclass

from configuration.ast.natives.AbstractNative import AbstractNative
from configuration.ast.natives.Float import Float
from configuration.ast.natives.Int import Int
from configuration.ast.natives.String import String
from configuration.ast.objects.AbstractType import AbstractType
from configuration.ast.objects.Dict import Dict
from configuration.ast.objects.List import List
from configuration.ast.objects.Value import Value

T = typing.TypeVar('T')


class Object(AbstractType):

    representation: T
    name: str
    wrapped_class: T
    wrapped_constructor: typing.Callable
    constructorParams: typing.Dict[str, AbstractType] = dict()

    def get_representative(self):
        pass

    def get_name(self) -> str:
        pass

    def get_native(self) -> typing.Type[AbstractNative]:
        pass

    def createObject(self) -> T:
        pass

    def __init__(self, tree: T):
        self.wrapped_class = tree
        self.wrapped_constructor = tree.__init__
        constructortypes = typing.get_type_hints(self.wrapped_constructor)

        for param in constructortypes:
            typee = constructortypes[param]
            print("%s has type %s" % (param, typee))
            self.constructorParams[param] = Object.parse_inner_type(typee)
            # Check for natives
        return

    def __repr__(self) -> str:
        return "Object{class: %s, nested: %s}" % (self.wrapped_class.__name__, self.constructorParams)

    @staticmethod
    def parse_inner_type(typee: typing.Any) -> typing.Type[AbstractType]:

        if typee == int:
            return Value(Int())
        elif typee == float:
            return Value(Float())
        elif typee == str:
            return Value(String())
        elif isinstance(typee, typing._GenericAlias):
            if typee._name == "Dict":
                return Dict(key=Object.parse_inner_type(typee.__args__[0]), value=Object.parse_inner_type(typee.__args__[1]))
                pass
            elif typee._name == "List":
                return List(Object.parse_inner_type(typee.__args__[0]))
                pass
        elif isclass(typee):
            return Object(typee)




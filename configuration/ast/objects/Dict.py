import typing

from configuration.ast.natives.AbstractNative import AbstractNative
from configuration.ast.objects.AbstractType import AbstractType, T


class Dict(AbstractType):
    key: typing.Type[AbstractType]
    value: typing.Type[AbstractType]

    def get_representative(self):
        pass

    def get_name(self) -> str:
        pass

    def get_native(self) -> AbstractNative:
        pass

    def createObject(self) -> T:
        pass

    def __init__(self, key: typing.Type[AbstractType], value: typing.Type[AbstractType]):
        self.key = key
        self.value = value

    def __repr__(self) -> str:
        return "Dict{key = %s, value = %s}" % (self.key, self.value)
from typing import Type

from configuration.ast.natives.AbstractNative import AbstractNative
from configuration.ast.objects.AbstractType import AbstractType, T


class Value(AbstractType):
    native: Type[AbstractNative]

    def get_representative(self):
        pass

    def get_name(self) -> str:
        pass

    def get_native(self) -> AbstractNative:
        pass

    def createObject(self) -> T:
        pass

    def __init__(self, native: Type[AbstractNative]):
        self.native = native

    def __repr__(self) -> str:
        return "Value{%s}" % self.native

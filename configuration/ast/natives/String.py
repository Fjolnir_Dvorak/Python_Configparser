from configuration.ast.natives.AbstractNative import AbstractNative


class String(AbstractNative):
    def __repr__(self) -> str:
        return "String"
    def append_to_stream(self):
        pass

    def to_string(self) -> str:
        pass

    def get_size(self) -> int:
        pass
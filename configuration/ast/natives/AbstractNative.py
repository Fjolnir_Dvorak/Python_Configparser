from abc import abstractmethod


class AbstractNative:

    @abstractmethod
    def append_to_stream(self):
        pass

    @abstractmethod
    def to_string(self) -> str:
        pass

    @abstractmethod
    def get_size(self) -> int:
        pass

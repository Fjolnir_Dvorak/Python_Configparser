from typing import Any, Callable, TypeVar

from configuration.ast.objects.Object import Object

T = TypeVar('T')

def parse(parse: str, inputtype: str, to: T) -> T:
    class_constructor: Object = Object